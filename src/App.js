import React from 'react';
import jwtDecode from 'jwt-decode';
import { AuthProvider, AuthService, useAuth } from 'react-oauth2-pkce';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';

const authService = new AuthService({
  clientId: process.env.REACT_APP_OAUTH2_CLIENT_ID,
  provider: `${process.env.REACT_APP_PROVIDER_URL}/oauth2`,
  redirectUri: 'http://localhost:3000',
  scopes: [],
});

const queryClient = new QueryClient();

const fetchUserData = async (id) => {
  const jwt = authService.getAuthTokens();
  const response = await fetch(`${process.env.REACT_APP_PROVIDER_URL}/api/v3/users/${id}`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Authorization': `${jwt.token_type} ${jwt.access_token}`,
    },
  });
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response.json();
};

const LoggedIn = () => {
  const { isLoading, error, data } = useQuery(['userData', 'me'], () => fetchUserData('me'));

  if (isLoading) {
    return (
      <p>Loading...</p>
    );
  }

  if (error) {
    return (
      <p>An error has occurred: {error.message}</p>
    );
  }

  return (
    <p>Logged in: {data.name} [{data.username}]</p>
  );
};

const App = () => {
  const { authService } = useAuth();

  const login = async () => authService.authorize();
  const logout = async () => authService.logout();

  if (authService.isPending()) {
    return (
      <div>
        <p>Please wait...</p>
        <button onClick={logout}>Reset</button>
      </div>
    );
  }

  if (!authService.isAuthenticated()) {
    return (
      <div>
        <p>Not Logged in yet</p>
        <button onClick={login}>Login</button>
      </div>
    );
  }

  const jwt = authService.getAuthTokens();
  const jwtPayload = jwtDecode(jwt.access_token);

  return (
    <div>
      <p>jwtPayload.sub: {jwtPayload.sub}</p>
      <LoggedIn />
      <button onClick={logout}>Logout</button>
    </div>
  );
}

export default () => (
  <AuthProvider authService={authService}>
    <QueryClientProvider client={queryClient}>
      <h1>
        react-oauth2-pkce-app-skeleton
      </h1>
      <App />
    </QueryClientProvider>
  </AuthProvider>
);
